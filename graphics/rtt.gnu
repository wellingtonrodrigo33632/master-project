reset
set terminal postscript eps size 20cm,15cm enhanced color solid font 'Helvetica,20' linewidth 2
set output 'rtt.eps'
unset key
unset logscale
set style line 1 lw 2 lc rgb 'gray'
set style data histogram
set style histogram errorbars gap 10 lw 1
set bars fullwidth
set mytics 5
set ytics 0.5
set grid mytics
set grid ytics
set grid back ls 1
set boxwidth 5 relative
set style fill solid 1.00 border 0
set title 'Round Trip Time Average (RTT)'
set xlabel 'Nodes Number'
set ylabel 'Round Trip Time(segundos)'
plot '/mnt/d/welli/Dropbox/Dropbox/Mestrado/RedesSemFio/Trabalho/program/resultados.txt' u (column(2)):(column(3)):xtic(1) t 'RTT Average' linecolor rgb '#0000FF';
unset output
