/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2008 INRIA
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Wellington Rodrigo de Freitas Costa <wellington@ime.uerj.br>

 *  F(x)=E^-((x-M)²/(2D²))/(D(2Pi)²)
 *
 * V=SUM(x-M)^2/N =>> V^2=D
 *
 * e=ZD/RAIZ(N) <=> N=(ZD/e)^2
 *
 * S=RAIZ(SUM(x-M)^2/N-1)
 */
#ifndef CALCULOS_HPP
#define CALCULOS_HPP
#include <cmath>
#include <stdlib.h>
#include <vector>

using namespace std;

class calculos {
public:
     calculos():N(0),soma(0),M(0),V(0),D(0),e(0){setZ(95);};
     calculos(uint16_t trust):N(0),soma(0),M(0),V(0),D(0),e(0){setZ(trust);};
     virtual ~calculos(){}
     
     void pushValor(double valor){
          if(valor>0){
               valores.push_back(valor);
               soma+=valor;
          }
     };//insere os valores da amostra caso o valor informado seja maior que 0
     double getMedia(){
          if(!valores.empty()){
               M=soma/valores.size();
          }
          return M;
     };//caso a lista de valores não esteja vazia, calcula a média dos valores já inseridos
     double getDesvio(){
          if(getMedia()>0){
               for(auto it = valores.begin(); it!=valores.end(); ++it){V=V+pow(*it-M,2);}
               V=V/valores.size();
               D=sqrt(V);
          }
          return D;
     };//caso a média seja diferente de zero (nenhuma média foi calculada), calcula o desvio padrão baseado na média e retorna.
     double getErro(){
           if(getDesvio()>0){
               e=(z*D/sqrt(valores.size()))/M;//erro porcentagem da Média
          }
          return e;
     };//calcula o erro para as amostras informadas
     /*para erro entre 0<erro<1 (0%<erro<100% da média) retorna a quantidade necessária de amostras para se ter a confiança necessária
     para valores fora retorna a quantidade de amostras já inseridas*/
     uint32_t getN(double erro){
          if((erro>0)&&(erro<1)){
               if(getDesvio()>0){
                    N=pow(((z*D)/(erro*M)),2);//erro porcentagem da Média
                    return N;
               }
          }
          return (uint32_t)valores.size();
     };
     void limpar(){
         N=0;
         soma=0;
         M=0;
         V=0;
         D=0;
         e=0;
         valores.clear();
     };//limpa as variáveis para um novo conjunto de amostras
     uint16_t setZ(uint16_t interval){
        switch(interval){
          case 70:
            z=1.04;
            return 70;
          case 75:
            z=1.15;
            return 75;
          case 80:
            z=1.28;
            return 80;
          case 85:
            z=1.44;
            return 85;
          case 90:
            z=1.645;
            return 90;
          case 92:
            z=1.75;
            return 92;
          case 96:
            z=2.05;
            return 96;
          case 98:
            z=2.33;
            return 98;
          case 99:
            z=2.58;
            return 99;
          default:
            z=1.96;
            return 95;
        };
     }
     
private:
     int N;//número de amostras
     /*soma,média,variância,desvio padrão,erro,coeficiente para intervalo de confiança*/
     double soma,M,V,D,e,z;
     vector<double> valores;//vetor de valores
};
#endif /* CALCULOS_HPP */