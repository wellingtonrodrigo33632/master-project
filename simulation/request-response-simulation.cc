/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2014 Universita' di Firenze
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Wellington Rodrigo de Freias Costa <wellington@ime.uerj.br>
 */
/*        Topologia da Rede WiFi Ad-hoc    
 *   n0      n1      n2........nN
 *   CC      |       |          |
 *   |<----->|<----->|<-------->|
 *   |<------------->|<-------->|
 *   |<------------------------>|
 *    
 *  
 *  CC - Control Center
 *  nk - Nó k (de 0 a N)
 *  n0 é o nó de controle e se comunica com todos os outros nós
 *
 *  Tracing of queues and packet receptions to file "request-response.tr"
 *
 */ 

#include "request-response-simulation.h"
#include <fstream>
#include <iomanip>
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>

using namespace std;

int 
main (int argc, char *argv[]){

  //Variabels to control execution
  uint32_t nNodes=10;//Specify number of nodes to create
  uint16_t portControl=1025;//Specify a port to connect with central
  uint16_t packetSize=1400;//Specify the size of message to send
  double timeout=1.0;//Define timeout for to send other packet if not received acknowledge
  uint16_t intervaltrust=95;//Define interval trust to gerate graph
  bool verbose=false;//verbose mode
  double time=60.0;//time execution
  uint32_t maxPackets=1000;//max packets to send
  uint16_t modeExecution=0;//0 by time mode, 1 by maximum packets received and 2 by samples to reach interval trust
  uint32_t rounds=1;
  uint64_t round=0;
  uint32_t variations=1;

  CommandLine cmd;
  cmd.AddValue ("verbose", "Ativate verbose mode if true.", verbose);
  cmd.AddValue("Nodes","Number of nodes to create (Min:1 and Max:253)",nNodes);
  cmd.AddValue("PortControl","Control Center port (Min:1024 and Max:65535)",portControl);
  cmd.AddValue("PacketSize","Size of message to send (Max:1275)",packetSize);
  cmd.AddValue("Timeout","Timeout of re-send packet if not received echo.",timeout);
  cmd.AddValue("IntervalTrust","Interval of trust of the simulation.",intervaltrust);
  cmd.AddValue("SimulingTime","Duration of the simulation in seconds.",time);
  cmd.AddValue("MaxPackets","The Maximum number of packets the application will send",maxPackets);
  cmd.AddValue("ExecutionMode","Execution mode (0 - by time 1 - by packets received 2 - by samples)",modeExecution);
  cmd.AddValue("Rounds","Rounds to execute simulation.",rounds);
  cmd.AddValue("Round","Start round.",round);
  cmd.AddValue("VariationNodesNumber","Number of variations of the nodes number.",variations);
  cmd.Parse (argc, argv);

    
  if (nNodes > 250 || nNodes < 1){
    cout << "Number of devices allowed: maximum 250 and minimum 1!!!" << endl;
    return EXIT_FAILURE;
  }
  //Verify the port number is greater 65535 and less than 1024
  if (portControl > 65535 || portControl < 1024){
    cout << "Port number allowed: maximum 65535 and minimum 1024!!!" << endl;
    return EXIT_FAILURE;
  }
  //LogComponentEnable ("RequestResponseSimulation", LOG_LEVEL_INFO);
  simulation simulated(round,nNodes,portControl,packetSize,timeout,intervaltrust,verbose,time,modeExecution,maxPackets);
  double executedtime=0;
  fstream file;
  char str[256];
  if (variations+rounds > 10 ){
    file.open("resultados.txt",fstream::out | fstream::in| fstream::app);
    if (file.is_open()){
      file.get(str,256);
      if(file.gcount() != 0){
        file  << "NODES \t RTT \t\t RTTE \t\t PDR \t\t PDRE" << endl;  
      }
      file << fixed << setprecision(6);
    }else{
      cout << "Error to open file!!!" << endl;
      return EXIT_FAILURE;
    }
  }
  if (variations > 0){
    if (rounds > 0){
      for (uint32_t i=nNodes; i < (nNodes+variations);++i){
        simulated.setNodesNumber(i);
        executedtime+=simulated.ExecuteSimulation(rounds);
        if (variations+rounds > 10 ){
          if (file.is_open()){
            file  << i << " \t "
                  << simulated.getRttAverage() << " \t "
                  << simulated.getErrorAverage()*simulated.getRttAverage() << " \t "
                  << simulated.getPdrAverage()*100 << " \t "
                  << simulated.getPdrErrorAverage()*simulated.getPdrAverage()*100 << endl;
          }else{
            cout << VERMELHO << "Result simulation nodes:" << fixed << setprecision(6)
                  << i << "\t RTT: "
                  << simulated.getRttAverage() << "\t RTTE: "
                  << simulated.getErrorAverage()*simulated.getRttAverage() << "\t PDR: "
                  << simulated.getPdrAverage()*100 << "\t PDRE: "
                  << simulated.getPdrErrorAverage()*simulated.getPdrAverage()*100 << endl;
            cin.get();
          }
        }else{
          cout << NORMAL << "Result simulation nodes: " << fixed << setprecision(6)
                  << nNodes << "\t RTT: "
                  << simulated.getRttAverage() << "\t RTTE: "
                  << simulated.getErrorAverage()*simulated.getRttAverage() << "\t PDR: "
                  << simulated.getPdrAverage()*100 << "%\t PDRE: "
                  << simulated.getPdrErrorAverage()*simulated.getPdrAverage()*100 << "%" << endl;
        }
      }
      cout << NORMAL << "Total Time of Simulation " << executedtime << "s" << endl;
    }
  }
   if (file.is_open()){
    file.close();
   }
   return EXIT_SUCCESS;
}
 /*
   * Calculate
   * error=((z*getDeviation())/sqrt(getSamples()))/getAverage()
   * z=(getError()*getAverage()*sqrt(getSamples()))/getDeviation()
   * N=pow(((z*getDeviation())/(getError()*getAverage())),2)
   * Interval Trust:z
   * 70:z=1.04
   * 75:z=1.15
   * 80:z=1.28
   * 85:z=1.44
   * 90:z=1.645
   * 92:z=1.75
   * 95:z=1.96
   * 96:z=2.05
   * 98:z=2.33
   * 99:z=2.58
   */