/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2014 Universita' di Firenze
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Wellington Rodrigo de Freias Costa <wellington@ime.uerj.br>
 */
/*        Topologia da Rede WiFi Ad-hoc    
 *   n0      n1      n2........nN
 *   CC      |       |          |
 *   |<----->|<----->|<-------->|
 *   |<------------->|<-------->|
 *   |<------------------------>|
 *    
 *  
 *  CC - Control Center
 *  nk - Nó k (de 0 a N)
 *  n0 é o nó de controle e se comunica com todos os outros nós
 *
 *  Tracing of queues and packet receptions to file "request-response.tr"
 *
 */ 
#ifndef REQUEST_RESPONSE_SIMULATION_H
#define REQUEST_RESPONSE_SIMULATION_H
#ifndef VERMELHO
#define VERMELHO "\033[0;31m"
#endif

#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/applications-module.h"
#include "ns3/mobility-module.h"
#include "ns3/stats-module.h"
#include "ns3/wifi-module.h"
#include "ns3/internet-module.h"
#include "ns3/aodv-module.h"
#include "ns3/calculos.h"
#include <cstdlib>
#include <cmath>
#include <time.h>
#include <iomanip>

#define SEED 20172
#define AMARELO "\033[1;33m" 
#define NORMAL "\033[0m"

using namespace ns3;

NS_LOG_COMPONENT_DEFINE("RequestResponseSimulation");
/*static void CourseChange(std::string foo, Ptr<const MobilityModel> mobility){
	Vector pos = mobility->GetPosition();
	Vector vel = mobility->GetVelocity();
	std::cout << Simulator::Now () << " Model=" << mobility
						<< "-POS:(" << pos.x << "," << pos.y << ")"
						<< "-VEL:(" << vel.x << "," << vel.y << ")" << std::endl;
};*/

class results{
	public:
		results(){
			rtt_m=0;
			error_m=0;
  		count=0;
		};
		~results(){};
		void clear(void){
			rtt_m=0;
			error_m=0;
  		count=0;
		};
		void putResult(double rtt, double error){
			rtt_m+=rtt;
			error_m+=error;
  		++count;
		};
		double getAverage(void){
			if(count==0)return 0;
			else return rtt_m/count;
		};
		double getError(void){
			if(count==0)return 0;
			else return error_m/count;
		};
	private:
		double rtt_m=0;
		double error_m=0;
  	uint32_t count=0;
};


class simulation{
	public:
		simulation( uint64_t round=0,
								uint32_t Nodes=10,  
							  uint16_t portControl=1025,
							  uint16_t packetSize=1400,
							  double timeout=1.0,
							  uint16_t intervaltrust=95,
							  bool verbose=false,
							  double time=60.0,
							  uint16_t mode=0,
							  uint32_t maxPackets=1000):
								m_round(round),
							  m_Nodes(Nodes),
							  m_maxPackets(maxPackets),
							  m_portControl(portControl),
							  m_packetSize(packetSize),
							  m_timeout(timeout),
							  m_verbose(verbose),
							  m_time(time),
							  m_mode(mode){
			m_intervaltrust=m_pdr.setZ(intervaltrust);
			m_rtt.clear();
			m_pdr.limpar();
		};

		~simulation(){};

		void setNodesNumber(uint32_t nodes){
			m_Nodes=nodes;
			m_rtt.clear();
			m_pdr.limpar();
		};
		
		//Execution rounds simulations modifing the round to run
		double ExecuteSimulation(uint32_t rounds);
		//Get results Average RTT and ERROR
		double getRttAverage(void){return m_rtt.getAverage();};
		double getErrorAverage(void){return m_rtt.getError();};
		//Get results Average PDR and ERROR
		double getPdrAverage(void ){return m_pdr.getMedia();};
		double getPdrErrorAverage(void){return m_pdr.getErro();};
	private:
		//caculate time of the execution
		double calcTimeExecution(timespec start, timespec finish);
		void configure(void);
		double ExecuteSimulation(void);

		uint64_t m_round;
		uint32_t m_Nodes; 
		uint32_t m_maxPackets; 
	  uint16_t m_portControl;
	  uint16_t m_packetSize;
	  double m_timeout;
	  uint16_t m_intervaltrust;
	  bool m_verbose;
	  double m_time;
	  uint16_t m_mode;
	  
	  results m_rtt;
	  calculos m_pdr;
	  
	  NodeContainer        m_nodes;
	  NodeContainer        m_serverNode;
	  NodeContainer        m_clientsNode;
	  NetDeviceContainer   m_nodesNet;
	  NetDeviceContainer   m_serverNet;
	  NetDeviceContainer   m_clientsNet;
	  Address              m_serverAddress;
	  ApplicationContainer m_clientsApp;
	  ApplicationContainer m_serverApp;
};

double
simulation::ExecuteSimulation(void){
	timespec start,finish;
	configure();
	m_serverApp.Start (Seconds (1.0));
  m_clientsApp.Start (Seconds (2.0));

  switch(m_mode){
    case 2:
      NS_LOG_INFO(NORMAL << "Run Simulation until the number of samples to reach interval trust of the " << (uint32_t)m_intervaltrust);
      break;
    case 1:
      NS_LOG_INFO(NORMAL << "Run Simulation until received " << m_maxPackets <<" packets");
      break;
    default:
      NS_LOG_INFO(NORMAL << "Run Simulation for " << m_time << " seconds");
      m_clientsApp.Stop (Seconds (m_time));
      m_serverApp.Stop (Seconds (m_time+1.0));
      Simulator::Stop(Seconds (m_time+2.0));
  };

	if(clock_gettime(CLOCK_REALTIME/*PROCESS_CPUTIME_ID*/, &start)!=0){return -1.0;}
  Simulator::Run ();
  Simulator::Destroy ();
  if(clock_gettime(CLOCK_REALTIME/*PROCESS_CPUTIME_ID*/, &finish)!=0){return -1.0;}
  for (ApplicationContainer::Iterator i=m_clientsApp.Begin(); i!=m_clientsApp.End(); ++i){
  	m_rtt.putResult(
  		(*i)->GetObject<RequestResponseClient>()->getRttAverage(),
  		(*i)->GetObject<RequestResponseClient>()->getRttError());
		m_pdr.pushValor((*i)->GetObject<RequestResponseClient>()->getPdr());
  }
  return calcTimeExecution(start,finish);
};

double
simulation::ExecuteSimulation(uint32_t rounds){
	double time;
	timespec start,finish;
	if(clock_gettime(CLOCK_REALTIME/*PROCESS_CPUTIME_ID*/, &start)!=0){return -1.0;}
	for (uint64_t i = m_round; i < (rounds+m_round); ++i){
		RngSeedManager::SetRun(i);
		std::cout << AMARELO << "Simulation with Nodes number:" << m_Nodes << " Round:" << i << std::endl;
		time = ExecuteSimulation();
		if(time < 0)return -1.0;
		NS_LOG_INFO(AMARELO << "Simulation number " << (i-m_round+1) << " finished in " << time << "s");
	}
	if(clock_gettime(CLOCK_REALTIME/*PROCESS_CPUTIME_ID*/, &finish)!=0){return -1.0;}
	return calcTimeExecution(start,finish);
};

double
simulation::calcTimeExecution(timespec start, timespec finish) {
    double tempo;
    timespec temp;
    if ((finish.tv_nsec-start.tv_nsec)<0) {
        temp.tv_sec = finish.tv_sec-start.tv_sec-1;
        temp.tv_nsec = 1000000000+finish.tv_nsec-start.tv_nsec;
    } else {
        temp.tv_sec = finish.tv_sec-start.tv_sec;
        temp.tv_nsec = finish.tv_nsec-start.tv_nsec;
    }
    tempo=temp.tv_sec+(temp.tv_nsec*0.0000000001);
    return tempo;
};
void
simulation::configure(void){
	m_nodes         = NodeContainer();
	m_serverNode    = NodeContainer();
	m_clientsNode   = NodeContainer();
	m_nodesNet      = NetDeviceContainer();
	m_serverNet     = NetDeviceContainer();
	m_clientsNet    = NetDeviceContainer();
	m_serverAddress = Address();
	m_clientsApp    = ApplicationContainer();
	m_serverApp     = ApplicationContainer();

	/*Config::SetDefault ("ns3::RandomWalk2dMobilityModel::Mode", StringValue ("Time"));
	Config::SetDefault ("ns3::RandomWalk2dMobilityModel::Time", StringValue ("10s"));
	Config::SetDefault ("ns3::RandomWalk2dMobilityModel::Speed", StringValue ("ns3::ConstantRandomVariable[Constant=2.0]"));
	Config::SetDefault ("ns3::RandomWalk2dMobilityModel::Bounds", StringValue ("0|1000|0|1000"));*/

	if (m_Nodes > 250){
		m_Nodes=250;
	}
	if(m_Nodes <1){
		m_Nodes=1;
	}
	if (m_verbose){
		LogComponentEnable("RequestResponseClientApplication", LOG_LEVEL_INFO);
	  LogComponentEnable("RequestResponseServerApplication", LOG_LEVEL_INFO);
	  LogComponentEnable("RequestResponseSimulation", LOG_LEVEL_INFO);
	}
	RngSeedManager::SetSeed(SEED);

	m_serverNode.Create(1);
	m_clientsNode.Create(m_Nodes);

	m_nodes.Add(m_serverNode);
	m_nodes.Add(m_clientsNode);

	InternetStackHelper internet;
	AodvHelper protocol;
/*protocol.Set("HelloInterval", Seconds(m_timeout*2.0));
  protocol.Set("ActiveRouteTimeout", Seconds(m_timeout*2.0));
  protocol.Set("NodeTraversalTime", MilliSeconds(m_timeout*100));
	protocol.Set("HelloInterval", Seconds(m_m_timeout+1.0));*/
	internet.SetRoutingHelper(protocol);
	internet.Install (m_nodes);

	WifiHelper wifi;
	wifi.SetRemoteStationManager("ns3::IdealWifiManager");
	wifi.SetStandard (WIFI_PHY_STANDARD_80211g);

	WifiMacHelper wifiMac;
	wifiMac.SetType("ns3::AdhocWifiMac");

	YansWifiChannelHelper channel = YansWifiChannelHelper::Default ();
	YansWifiPhyHelper phy         = YansWifiPhyHelper::Default ();
	phy.SetChannel (channel.Create ());	
	
	m_serverNet  = wifi.Install (phy, wifiMac, m_serverNode);
	m_clientsNet = wifi.Install (phy, wifiMac, m_clientsNode);

	m_nodesNet.Add(m_serverNet);
	m_nodesNet.Add(m_clientsNet);

	MobilityHelper mobility;
	mobility.SetPositionAllocator ("ns3::RandomDiscPositionAllocator",
	                                "X", StringValue("500.0"),
	                                "Y", StringValue("500.0"),
	                                "Rho", StringValue("ns3::UniformRandomVariable[Min=0|Max=150]"));
	mobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
	mobility.Install (m_serverNode);
	mobility.SetMobilityModel ("ns3::RandomWalk2dMobilityModel",
														 "Mode", StringValue ("Time"),
													   "Time", StringValue ("5s"),
													   "Speed", StringValue ("ns3::ConstantRandomVariable[Constant=2.0]"),
													   "Bounds", StringValue ("0|1000|0|1000"));
	mobility.Install (m_clientsNode);

  Ipv4AddressHelper ipv4;
  ipv4.SetBase ("10.1.1.0", "255.255.255.0");
  Ipv4InterfaceContainer i4 = ipv4.Assign (m_nodesNet);
  m_serverAddress = Address(i4.GetAddress (0));

	RequestResponseServerHelper control(m_portControl);
	m_serverApp = control.Install(m_serverNode);
	RequestResponseClientHelper devices (m_serverAddress, m_portControl);
	devices.SetAttribute("ExecutionMode", UintegerValue(m_mode));
	devices.SetAttribute("Timeout",       TimeValue(Seconds (m_timeout)));
	devices.SetAttribute("MaxPackets",    UintegerValue(m_maxPackets));
	devices.SetAttribute("PacketSize",    UintegerValue(m_packetSize));
	devices.SetAttribute("IntervalTrust", UintegerValue(m_intervaltrust));
	m_clientsApp = devices.Install (m_clientsNode);
};
#endif /* REQUEST_RESPONSE_SIMULATION_H */