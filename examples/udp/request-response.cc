/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2014 Universita' di Firenze
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Wellington Rodrigo de Freias Costa <wellington@ime.uerj.br>
 */
/*        Topologia da Rede WiFi Ad-hoc    
 *   n0      n1      n2........nN
 *   CC      |       |          |
 *   |<----->|<----->|<-------->|
 *   |<------------->|<-------->|
 *   |<------------------------>|
 *    
 *  
 *  CC - Control Center
 *  nk - Nó k (de 0 a N)
 *  n0 é o nó de controle e se comunica com todos os outros nós
 *
 *  Tracing of queues and packet receptions to file "request-response.tr"
 *
 */ 

#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/applications-module.h"
#include "ns3/mobility-module.h"
#include "ns3/stats-module.h"
#include "ns3/wifi-module.h"
#include "ns3/internet-module.h"
#include "ns3/aodv-module.h"
//for basic programming
#include <cstdlib>
#include <iostream>
#include <iomanip>
#include <string>
#include <sstream>
#include <cmath>
#include <time.h>
#define SEED 20172
#define VERMELHO  "\033[0;31m"
#define AMARELO   "\033[1;33m" 
#define NORMAL    "\033[0m"

using namespace ns3;
using namespace std;

NS_LOG_COMPONENT_DEFINE ("RequestResponseExample");
double calculateTime(timespec start, timespec end);

int 
main (int argc, char *argv[]){

  LogComponentEnable ("RequestResponseExample", LOG_LEVEL_INFO);

  //Variabels to control execution
  uint32_t nNodes=10;//Specify number of nodes to create
  uint16_t portControl=1025;//Specify a port to connect with central
  uint16_t packetSize=1400;//Specify the size of message to send
  double timeout=1.0;//Define timeout for to send other packet if not received acknowledge
  uint16_t intervaltrust=95;//Define interval trust to gerate graph
  bool verbose=false;//verbose mode
  double time=60.0;//time execution 60 seconds
  uint32_t maxPackets=1000;//max packets to send, if the mode is set to 0 it is ignored
  uint16_t modeExecution=0;//0 by time mode, 1 by maximum packets received and 2 by samples to reach interval trust
  uint32_t round=0;

  CommandLine cmd;
  cmd.AddValue ("verbose", "Ativate verbose mode if true.", verbose);
  cmd.AddValue("Nodes","Number of nodes to create (Min:1 and Max:253).",nNodes);
  cmd.AddValue("PortControl","Control Center port (Min:1024 and Max:65535).",portControl);
  cmd.AddValue("PacketSize","Size of message to send (Max:1275)",packetSize);
  cmd.AddValue("Timeout","Timeout of re-send packet if not received echo.",timeout);
  cmd.AddValue("IntervalTrust","Interval of trust of the simulation.",intervaltrust);
  cmd.AddValue("SimulingTime","Duration of the simulation in seconds.",time);
  cmd.AddValue("MaxPackets","The Maximum number of packets the application will send.",maxPackets);
  cmd.AddValue("ExecutionMode","Execution mode (0 - by time 1 - by packets received 2 - by samples).",modeExecution);
  cmd.AddValue("Round","Round to modify execution round.",round);
  cmd.Parse (argc, argv);

  if(verbose){
    LogComponentEnable ("RequestResponseClientApplication", LOG_LEVEL_INFO);
    LogComponentEnable ("RequestResponseServerApplication", LOG_LEVEL_INFO);
  }
  
  NS_LOG_INFO (AMARELO << "Create nodes.");
  if (nNodes > 250 || nNodes < 1){
    cout << VERMELHO << "Number of devices allowed: maximum 253 and minimum 1!!!" << endl;
    return EXIT_FAILURE;
  }
  //Verify the port number is greater 65535 and less than 1024
  if (portControl > 65535 || portControl < 1024){
    cout << VERMELHO << "Port number allowed: maximum 65535 and minimum 1024!!!" << endl;
    return EXIT_FAILURE;
  }
  RngSeedManager::SetSeed(SEED);
  RngSeedManager::SetRun(round);

  NodeContainer ccNode;//reference to CC node
  ccNode.Create(1);
  NodeContainer devNodes;//reference to devices nodes
  devNodes.Create(nNodes);
  NodeContainer allNodes;//reference to all nodes
  allNodes.Add(ccNode);
  allNodes.Add(devNodes);
  
  NS_LOG_INFO (AMARELO << "Define AODV routing protocol for Ad-Hoc mode");
  InternetStackHelper internet;
  AodvHelper protocol;
/*protocol.Set("HelloInterval", Seconds(timeout*2.0));
  protocol.Set("ActiveRouteTimeout", Seconds(timeout*2.0));
  protocol.Set("NodeTraversalTime", MilliSeconds(timeout*100));*/
  internet.SetRoutingHelper(protocol);
  internet.Install (allNodes);

  NS_LOG_INFO (AMARELO << "Create channels");
  WifiHelper wifi;
  wifi.SetRemoteStationManager("ns3::IdealWifiManager");
  wifi.SetStandard (WIFI_PHY_STANDARD_80211g);
  YansWifiChannelHelper channel = YansWifiChannelHelper::Default ();
  //Adicionar modelo de erros de propagação
  YansWifiPhyHelper phy = YansWifiPhyHelper::Default ();
  phy.SetChannel (channel.Create ());
  WifiMacHelper wifiMac;
  wifiMac.SetType("ns3::AdhocWifiMac");
  NetDeviceContainer ccDevice = wifi.Install (phy, wifiMac, ccNode);
  NetDeviceContainer devDevices = wifi.Install (phy, wifiMac, devNodes);
  NetDeviceContainer allDevices;
  allDevices.Add(ccDevice);
  allDevices.Add(devDevices);

  NS_LOG_INFO (AMARELO << "Create Modility Model");
  MobilityHelper mobility;
  mobility.SetPositionAllocator ("ns3::RandomRectanglePositionAllocator",
                                  "X", StringValue("ns3::UniformRandomVariable"),
                                  "Y", StringValue("ns3::UniformRandomVariable"));
  mobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
  mobility.Install (ccNode);
  mobility.SetMobilityModel ("ns3::RandomWalk2dMobilityModel","Bounds", RectangleValue (Rectangle (-10000, 10000, -10000, 10000)));
  mobility.Install (devNodes);
  
  NS_LOG_INFO (AMARELO << "Assign IP Addresses");
  Address controlAddress;
  Ipv4AddressHelper ipv4;
  ipv4.SetBase ("10.1.1.0", "255.255.255.0");
  Ipv4InterfaceContainer i4 = ipv4.Assign (allDevices);
  controlAddress = Address(i4.GetAddress (0));

  NS_LOG_INFO (AMARELO << "Create Applications");

  RequestResponseServerHelper control(portControl);
  ApplicationContainer controlApp = control.Install(ccNode);

  RequestResponseClientHelper devices (controlAddress, portControl);
  devices.SetAttribute("ExecutionMode",  UintegerValue (modeExecution));
  devices.SetAttribute("Timeout",  TimeValue(Seconds (timeout)));
  devices.SetAttribute("MaxPackets",  UintegerValue (maxPackets));
  devices.SetAttribute("PacketSize",  UintegerValue (packetSize));
  devices.SetAttribute("IntervalTrust",  UintegerValue (intervaltrust));
  ApplicationContainer devApp = devices.Install (devNodes);
  
  controlApp.Start (Seconds (1.0));
  devApp.Start (Seconds (2.0));

  switch(modeExecution){
    case 2:
      NS_LOG_INFO(AMARELO << "Run Simulation until the number of samples to reach interval trust of the " << intervaltrust);
      break;
    case 1:
      NS_LOG_INFO(AMARELO << "Run Simulation until received" << maxPackets <<" packets");
      break;
    default:
      NS_LOG_INFO(AMARELO << "Run Simulation for " << time << " seconds");
      devApp.Stop (Seconds (time));
      controlApp.Stop (Seconds (time+1.0));
      Simulator::Stop(Seconds (time+2.0));
  };
    
  timespec inicio,fim;
  //adicionar tempo de execucao da simulacao
  if(clock_gettime(CLOCK_REALTIME/*PROCESS_CPUTIME_ID*/, &inicio)!=0){exit(EXIT_FAILURE);}
  Simulator::Run ();
  Simulator::Destroy ();
  if(clock_gettime(CLOCK_REALTIME/*PROCESS_CPUTIME_ID*/, &fim)!=0){exit(EXIT_FAILURE);}
  NS_LOG_INFO (AMARELO << "Done in " << calculateTime(inicio,fim) << "s");
  /*
   * Calculate
   * error=((z*getDeviation())/sqrt(getSamples()))/getAverage()
   * z=(getError()*getAverage()*sqrt(getSamples()))/getDeviation()
   * N=pow(((z*getDeviation())/(getError()*getAverage())),2)
   * Interval Trust:z
   * 70:z=1.04
   * 75:z=1.15
   * 80:z=1.28
   * 85:z=1.44
   * 90:z=1.645
   * 92:z=1.75
   * 95:z=1.96
   * 96:z=2.05
   * 98:z=2.33
   * 99:z=2.58
   */
  double rtt=0;
  double pdr=0;
  double error=0;
  uint32_t count=0;

  for(ApplicationContainer::Iterator i=devApp.Begin(); i!=devApp.End(); ++i){
    ++count;
    rtt+=(*i)->GetObject<RequestResponseClient>()->getRttAverage();
    error+=(*i)->GetObject<RequestResponseClient>()->getRttError();
    pdr+=(*i)->GetObject<RequestResponseClient>()->getPdr();
  }

  cout << NORMAL << std::fixed << std::setprecision(6);
  cout << "RTT médio: "  << rtt/count                   << "s" << endl
       << "Erro médio: " << (uint32_t)(error*100/count) << "%" << endl
       << "PDR médio: "  << (uint32_t)(pdr/count*100)   << "%" << endl;
}
double calculateTime(timespec start, timespec end) {
    double tempo;
    timespec temp;
    if ((end.tv_nsec-start.tv_nsec)<0) {
        temp.tv_sec = end.tv_sec-start.tv_sec-1;
        temp.tv_nsec = 1000000000+end.tv_nsec-start.tv_nsec;
    } else {
        temp.tv_sec = end.tv_sec-start.tv_sec;
        temp.tv_nsec = end.tv_nsec-start.tv_nsec;
    }
    tempo=temp.tv_sec+(temp.tv_nsec*0.0000000001);
    return tempo;
}